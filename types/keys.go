// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

const (
	// ModuleName defines the module name
	ModuleName = "Wallet"

	// StoreKey defines the primary module store key
	StoreKey = ModuleName

	// RouterKey is the message route for slashing
	RouterKey = ModuleName

	// QuerierRoute defines the module's query routing key
	QuerierRoute = ModuleName

	// MemStoreKey defines the in-memory store key
	MemStoreKey = "mem_capability"
)

func KeyPrefix(p string) []byte {
	return []byte(p)
}

const (
	WalletKey      = "Wallet-value-"
	WalletCountKey = "Wallet-count-"
)

const (
	SegmentKey      = "Segment-value-"
	SegmentCountKey = "Segment-count-"
	Inbox           = "Inbox"
	InboxInfo       = "Default Inbox Segment"
)

const (
	WalletHistoryKey      = "WalletHistory-value-"
	WalletHistoryCountKey = "WalletHistory-count-"
)

const (
	SegmentHistoryKey      = "SegmentHistory-value-"
	SegmentHistoryCountKey = "SegmentHistory-count-"
)

const (
	TokenHistoryGlobalKey      = "TokenHistoryGlobal-value-"
	TokenHistoryGlobalCountKey = "TokenHistoryGlobal-count-"
)

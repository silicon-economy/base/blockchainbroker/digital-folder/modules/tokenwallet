// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateSegmentHistory{}

func NewMsgCreateSegmentHistory(creator string, id string, history []*Segment) *MsgCreateSegmentHistory {
	return &MsgCreateSegmentHistory{
		Creator: creator,
		Id:      id,
		History: history,
	}
}

func (msg *MsgCreateSegmentHistory) Route() string {
	return RouterKey
}

func (msg *MsgCreateSegmentHistory) Type() string {
	return "CreateSegmentHistory"
}

func (msg *MsgCreateSegmentHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateSegmentHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateSegmentHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateSegmentHistory{}

func NewMsgUpdateSegmentHistory(creator string, id string) *MsgUpdateSegmentHistory {
	return &MsgUpdateSegmentHistory{
		Id:      id,
		Creator: creator,
	}
}

func (msg *MsgUpdateSegmentHistory) Route() string {
	return RouterKey
}

func (msg *MsgUpdateSegmentHistory) Type() string {
	return "UpdateSegmentHistory"
}

func (msg *MsgUpdateSegmentHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateSegmentHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateSegmentHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateTokenHistoryGlobal{}

func NewMsgCreateTokenHistoryGlobal(creator string) *MsgCreateTokenHistoryGlobal {
	return &MsgCreateTokenHistoryGlobal{
		Creator: creator,
	}
}

func (msg *MsgCreateTokenHistoryGlobal) Route() string {
	return RouterKey
}

func (msg *MsgCreateTokenHistoryGlobal) Type() string {
	return "CreateTokenHistoryGlobal"
}

func (msg *MsgCreateTokenHistoryGlobal) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateTokenHistoryGlobal) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateTokenHistoryGlobal) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateTokenHistoryGlobal{}

func NewMsgUpdateTokenHistoryGlobal(creator string, id string) *MsgUpdateTokenHistoryGlobal {
	return &MsgUpdateTokenHistoryGlobal{
		Id:      id,
		Creator: creator,
	}
}

func (msg *MsgUpdateTokenHistoryGlobal) Route() string {
	return RouterKey
}

func (msg *MsgUpdateTokenHistoryGlobal) Type() string {
	return "UpdateTokenHistoryGlobal"
}

func (msg *MsgUpdateTokenHistoryGlobal) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateTokenHistoryGlobal) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateTokenHistoryGlobal) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

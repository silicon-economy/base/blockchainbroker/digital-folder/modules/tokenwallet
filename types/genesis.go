// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"fmt"
	"time"
)

// DefaultIndex is the default capability global index
const DefaultIndex uint64 = 1

// DefaultGenesis returns the default Capability genesis state
func DefaultGenesis() *GenesisState {
	genesisAccount := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"
	return &GenesisState{
		// this line is used by starport scaffolding # genesis/types/default
		TokenHistoryGlobalList: []*TokenHistoryGlobal{},
		SegmentHistoryList:     []*SegmentHistory{},
		WalletHistoryList:      []*WalletHistory{},
		SegmentList: []*Segment{
			{
				Creator:   genesisAccount,
				Id:        "0",
				Name:      "Hash segment",
				Timestamp: time.Now().UTC().Format(time.RFC3339),
				Info:      "A segment for storing Hashes",
				WalletId:  "0",
				TokenRefs: []*TokenRef{},
			},
		},
		WalletList: []*Wallet{
			{
				Creator:    genesisAccount,
				Id:         "0",
				Name:       "Hash Wallet",
				Timestamp:  time.Now().UTC().Format(time.RFC3339),
				SegmentIds: []string{"0"},
				WalletAccounts: []*WalletAccount{
					{
						Address: genesisAccount,
						Active:  true,
					},
				},
			},
		},
	}
}

// Validate performs basic genesis state validation returning an error upon any failure
func (gs GenesisState) Validate() error {
	// this line is used by starport scaffolding # genesis/types/validate
	// Check for duplicated ID in tokenHistoryGlobal
	tokenHistoryGlobalIdMap := make(map[string]bool)

	for _, elem := range gs.TokenHistoryGlobalList {
		if _, ok := tokenHistoryGlobalIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for tokenHistoryGlobal")
		}
		tokenHistoryGlobalIdMap[elem.Id] = true
	}

	// Check for duplicated ID in segmentHistory
	segmentHistoryIdMap := make(map[string]bool)

	for _, elem := range gs.SegmentHistoryList {
		if _, ok := segmentHistoryIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for segmentHistory")
		}
		segmentHistoryIdMap[elem.Id] = true
	}

	// Check for duplicated ID in walletHistory
	walletHistoryIdMap := make(map[string]bool)

	for _, elem := range gs.WalletHistoryList {
		if _, ok := walletHistoryIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for walletHistory")
		}
		walletHistoryIdMap[elem.Id] = true
	}

	// Check for duplicated ID in segment
	segmentIdMap := make(map[string]bool)

	for _, elem := range gs.SegmentList {
		if _, ok := segmentIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for segment")
		}
		segmentIdMap[elem.Id] = true
	}

	// Check for duplicated ID in wallet
	walletIdMap := make(map[string]bool)

	for _, elem := range gs.WalletList {
		if _, ok := walletIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for wallet")
		}
		walletIdMap[elem.Id] = true
	}

	return nil
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateWalletHistory{}

func NewMsgCreateWalletHistory(creator string, id string, history []*Wallet) *MsgCreateWalletHistory {
	return &MsgCreateWalletHistory{
		Creator: creator,
		Id:      id,
		History: history,
	}
}

func (msg *MsgCreateWalletHistory) Route() string {
	return RouterKey
}

func (msg *MsgCreateWalletHistory) Type() string {
	return "CreateWalletHistory"
}

func (msg *MsgCreateWalletHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateWalletHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateWalletHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateWalletHistory{}

func NewMsgUpdateWalletHistory(creator string, id string) *MsgUpdateWalletHistory {
	return &MsgUpdateWalletHistory{
		Id:      id,
		Creator: creator,
	}
}

func (msg *MsgUpdateWalletHistory) Route() string {
	return RouterKey
}

func (msg *MsgUpdateWalletHistory) Type() string {
	return "UpdateWalletHistory"
}

func (msg *MsgUpdateWalletHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateWalletHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateWalletHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

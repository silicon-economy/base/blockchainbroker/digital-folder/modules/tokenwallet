<!--
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3
-->

# TokenWallet

**TokenWallet** is a blockchain module built using Cosmos SDK and Tendermint and created
with [ignite](https://github.com/ignite/cli).

## Cloning and updating of the repository

The **TokenWallet** repository is a blockchain module used by
the [TokenManger](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/tokenmanager)
.
The Token Manager uses the modules
[TokenWallet](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/modules/tokenwallet)
,
[Token](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/modules/token),
[BusinessLogic](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/modules/businesslogic)
and
[HashToken](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/modules/hashtoken)
.
To see how to clone and update all modules at once see the readme of the Token Manager.

## Documentation

The [documentation](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/documentation/-/blob/main/index.adoc)
provides extensive information about the Token Manager and its components, including the module **TokenWallet**.

## Installation and Configuration

To use the module **TokenWallet**
the [TokenManger](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/tokenmanager)
needs to be setup. The installation and setup of the TokenManager is described
in [chapter 12](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/documentation/-/blob/main/index.adoc)
of the documentation.

Your blockchain can be configured with `config.yml` during development. To learn more see
the [ignite documentation](https://github.com/ignite/cli#documentation).

## Launch

To launch your blockchain live on multiple nodes use `ignite network` commands. Learn more
about [Starport Network](https://github.com/tendermint/spn).

## CLI commands

For a list of the available CLI commands see the readMe of
the [TokenManger](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/tokenmanager)
.

## Learn more

- [ignite](https://github.com/ignite/cli)
- [Cosmos SDK documentation](https://docs.cosmos.network)
- [Cosmos SDK Tutorials](https://tutorials.cosmos.network)
- [Discord](https://discord.gg/W8trcGV)

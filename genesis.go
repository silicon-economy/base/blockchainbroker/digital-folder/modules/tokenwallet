// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package Wallet

import (
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

// InitGenesis initializes the capability module's state from a provided genesis state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, genState types.GenesisState) {
	k.InitGenesis(ctx, genState)
}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()

	// this line is used by starport scaffolding # genesis/module/export
	// Get all tokenHistoryGlobal
	tokenHistoryGlobalList := k.GetAllTokenHistoryGlobal(ctx)
	for _, elem := range tokenHistoryGlobalList {
		elem := elem
		genesis.TokenHistoryGlobalList = append(genesis.TokenHistoryGlobalList, &elem)
	}

	// Get all segmentHistory
	segmentHistoryList := k.GetAllSegmentHistory(ctx)
	for _, elem := range segmentHistoryList {
		elem := elem
		genesis.SegmentHistoryList = append(genesis.SegmentHistoryList, &elem)
	}

	// Get all walletHistory
	walletHistoryList := k.GetAllWalletHistory(ctx)
	for _, elem := range walletHistoryList {
		elem := elem
		genesis.WalletHistoryList = append(genesis.WalletHistoryList, &elem)
	}

	// Get all segment
	segmentList := k.GetAllSegment(ctx)
	for _, elem := range segmentList {
		elem := elem
		genesis.SegmentList = append(genesis.SegmentList, &elem)
	}

	// Get all wallet
	walletList := k.GetAllWallet(ctx)
	for _, elem := range walletList {
		elem := elem
		genesis.WalletList = append(genesis.WalletList, &elem)
	}

	return genesis
}

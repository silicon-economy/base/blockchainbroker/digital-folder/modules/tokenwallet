// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

// GetSegmentCount gets the total number of segment
func (k Keeper) GetSegmentCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentCountKey))
	byteKey := types.KeyPrefix(types.SegmentCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetSegmentCount sets the total number of segment
func (k Keeper) SetSegmentCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentCountKey))
	byteKey := types.KeyPrefix(types.SegmentCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendSegment appends a segment in the store with a new id and updates the count
func (k Keeper) AppendSegment(ctx sdk.Context, msg types.MsgCreateSegment) string {
	// Create the segment
	count := k.GetSegmentCount(ctx)
	id := strconv.FormatInt(count, 10)

	var segment = types.Segment{
		Creator:   msg.Creator,
		Id:        id,
		Name:      msg.Name,
		Timestamp: GetTimestamp(),
		Info:      msg.Info,
		WalletId:  msg.WalletId,
	}

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentKey))
	key := types.KeyPrefix(types.SegmentKey + segment.Id)
	value := k.cdc.MustMarshal(&segment)
	store.Set(key, value)

	// Update segment count
	k.SetSegmentCount(ctx, count+1)

	// Append the new SegmentId to the related Wallet Segment List
	k.AppendWalletSegmentIds(ctx, msg.WalletId, id)

	return id
}

// AppendSegmentWithId appends a segment in the store with a new id and updates the count
func (k Keeper) AppendSegmentWithId(ctx sdk.Context, msg types.MsgCreateSegmentWithId) string {
	// Create the segment
	count := k.GetSegmentCount(ctx)
	id := msg.Id

	var segment = types.Segment{
		Creator:   msg.Creator,
		Id:        id,
		Name:      msg.Name,
		Timestamp: GetTimestamp(),
		Info:      msg.Info,
		WalletId:  msg.WalletId,
	}

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentKey))
	key := types.KeyPrefix(types.SegmentKey + segment.Id)
	value := k.cdc.MustMarshal(&segment)
	store.Set(key, value)

	// Update segment count
	k.SetSegmentCount(ctx, count+1)

	// Append the new SegmentId to the related Wallet Segment List
	k.AppendWalletSegmentIds(ctx, msg.WalletId, id)

	return id
}

// SetSegment sets a specific segment in the store
func (k Keeper) SetSegment(ctx sdk.Context, segment types.Segment) {
	oldSegment := k.GetSegment(ctx, segment.Id)

	k.CreateSegmentHistoryEntry(ctx, oldSegment)

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentKey))
	b := k.cdc.MustMarshal(&segment)
	store.Set(types.KeyPrefix(types.SegmentKey+segment.Id), b)
}

// CreateSegmentHistoryEntry creates an entry in the segment history
func (k Keeper) CreateSegmentHistoryEntry(ctx sdk.Context, oldSegment types.Segment) {
	segmentHistoryExists := k.HasSegmentHistory(ctx, oldSegment.Id)

	if segmentHistoryExists {
		segmentHistory := k.GetSegmentHistory(ctx, oldSegment.Id)
		segmentHistory.History = append(segmentHistory.History, &oldSegment)
		k.SetSegmentHistory(ctx, segmentHistory)
	} else {
		segmentHistory := types.MsgCreateSegmentHistory{
			Creator: oldSegment.Creator,
			Id:      oldSegment.Id,
			History: []*types.Segment{
				&oldSegment,
			},
		}
		k.AppendSegmentHistory(ctx, segmentHistory)
	}
}

// GetSegment returns a segment from its id
func (k Keeper) GetSegment(ctx sdk.Context, key string) types.Segment {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentKey))
	var segment types.Segment
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.SegmentKey+key)), &segment)
	return segment
}

// HasSegment checks if the segment exists in the store
func (k Keeper) HasSegment(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentKey))
	return store.Has(types.KeyPrefix(types.SegmentKey + id))
}

// GetSegmentOwner returns the creator of the segment
func (k Keeper) GetSegmentOwner(ctx sdk.Context, key string) string {
	return k.GetSegment(ctx, key).Creator
}

// GetAllSegment returns all segment
func (k Keeper) GetAllSegment(ctx sdk.Context) (msgs []types.Segment) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.SegmentKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.Segment
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}

func (k Keeper) AppendTokenRef(ctx sdk.Context, msg types.MsgCreateTokenRef) {
	var tokenRef = types.TokenRef{
		Id:        msg.Id,
		ModuleRef: msg.ModuleRef,
		Valid:     true,
	}

	segment := k.GetSegment(ctx, msg.SegmentId)
	segment.TokenRefs = append(segment.TokenRefs, &tokenRef)

	k.SetSegment(ctx, segment)
}

func (k Keeper) MoveToken(ctx sdk.Context, msg types.MsgMoveTokenToSegment) {
	sourceSegment := k.GetSegment(ctx, msg.SourceSegmentId)
	var sourceTokenRefs []*types.TokenRef
	var tokenRef types.TokenRef
	for i, v := range sourceSegment.TokenRefs {
		if v.Id == msg.TokenRefId {
			tokenRef.Id = v.Id
			tokenRef.ModuleRef = v.ModuleRef
			tokenRef.Valid = v.Valid
			sourceTokenRefs = append(sourceSegment.TokenRefs[:i], sourceSegment.TokenRefs[i+1:]...)
		}
	}
	k.AppendTokenRef(ctx, types.MsgCreateTokenRef{
		Creator:   msg.Creator,
		Id:        tokenRef.Id,
		ModuleRef: tokenRef.ModuleRef,
		SegmentId: msg.TargetSegmentId,
	})
	sourceSegment.TokenRefs = sourceTokenRefs
	k.SetSegment(ctx, sourceSegment)
}

func (k Keeper) RemoveToken(ctx sdk.Context, msg types.MsgRemoveTokenRefFromSegment) {
	segment := k.GetSegment(ctx, msg.SegmentId)
	var sourceTokenRefs []*types.TokenRef
	for i, v := range segment.TokenRefs {
		if v.Id == msg.TokenRefId {
			sourceTokenRefs = append(segment.TokenRefs[:i], segment.TokenRefs[i+1:]...)
		}
	}
	segment.TokenRefs = sourceTokenRefs
	k.SetSegment(ctx, segment)
}

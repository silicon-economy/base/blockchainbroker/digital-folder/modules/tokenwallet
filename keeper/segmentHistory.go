// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

// GetSegmentHistoryCount gets the total number of segmentHistory
func (k Keeper) GetSegmentHistoryCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentHistoryCountKey))
	byteKey := types.KeyPrefix(types.SegmentHistoryCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetSegmentHistoryCount sets the total number of segmentHistory
func (k Keeper) SetSegmentHistoryCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentHistoryCountKey))
	byteKey := types.KeyPrefix(types.SegmentHistoryCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendSegmentHistory appends a segmentHistory in the store with a new id and updates the count
func (k Keeper) AppendSegmentHistory(ctx sdk.Context, msg types.MsgCreateSegmentHistory) string {
	// Create the segmentHistory
	count := k.GetSegmentHistoryCount(ctx)
	var segmentHistory = types.SegmentHistory{
		Creator: msg.Creator,
		Id:      msg.Id,
	}

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentHistoryKey))
	key := types.KeyPrefix(types.SegmentHistoryKey + segmentHistory.Id)
	value := k.cdc.MustMarshal(&segmentHistory)
	store.Set(key, value)

	// Update segmentHistory count
	k.SetSegmentHistoryCount(ctx, count+1)

	return msg.Id
}

// SetSegmentHistory sets a specific segmentHistory in the store
func (k Keeper) SetSegmentHistory(ctx sdk.Context, segmentHistory types.SegmentHistory) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentHistoryKey))
	b := k.cdc.MustMarshal(&segmentHistory)
	store.Set(types.KeyPrefix(types.SegmentHistoryKey+segmentHistory.Id), b)
}

// GetSegmentHistory returns a segmentHistory from its id
func (k Keeper) GetSegmentHistory(ctx sdk.Context, key string) types.SegmentHistory {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentHistoryKey))
	var segmentHistory types.SegmentHistory
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.SegmentHistoryKey+key)), &segmentHistory)
	return segmentHistory
}

// HasSegmentHistory checks if the segmentHistory exists in the store
func (k Keeper) HasSegmentHistory(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentHistoryKey))
	return store.Has(types.KeyPrefix(types.SegmentHistoryKey + id))
}

// GetSegmentHistoryOwner returns the creator of the segmentHistory
func (k Keeper) GetSegmentHistoryOwner(ctx sdk.Context, key string) string {
	return k.GetSegmentHistory(ctx, key).Creator
}

// GetAllSegmentHistory returns all segmentHistory
func (k Keeper) GetAllSegmentHistory(ctx sdk.Context) (msgs []types.SegmentHistory) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentHistoryKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.SegmentHistoryKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.SegmentHistory
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}

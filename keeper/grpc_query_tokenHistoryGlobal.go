// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k Keeper) TokenHistoryGlobalAll(c context.Context, req *types.QueryAllTokenHistoryGlobalRequest) (*types.QueryAllTokenHistoryGlobalResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	var tokenHistoryGlobals []*types.TokenHistoryGlobal
	store := ctx.KVStore(k.storeKey)
	tokenHistoryGlobalStore := prefix.NewStore(store, types.KeyPrefix(types.TokenHistoryGlobalKey))

	pageRes, err := query.Paginate(tokenHistoryGlobalStore, req.Pagination, func(key []byte, value []byte) error {
		var tokenHistoryGlobal types.TokenHistoryGlobal
		if err := k.cdc.Unmarshal(value, &tokenHistoryGlobal); err != nil {
			return err
		}

		tokenHistoryGlobals = append(tokenHistoryGlobals, &tokenHistoryGlobal)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllTokenHistoryGlobalResponse{TokenHistoryGlobal: tokenHistoryGlobals, Pagination: pageRes}, nil
}

func (k Keeper) TokenHistoryGlobal(c context.Context, req *types.QueryGetTokenHistoryGlobalRequest) (*types.QueryGetTokenHistoryGlobalResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)

	if !k.HasTokenHistoryGlobal(ctx, req.Id) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	tokenHistoryGlobal := k.GetTokenHistoryGlobal(ctx, req.Id)
	return &types.QueryGetTokenHistoryGlobalResponse{TokenHistoryGlobal: &tokenHistoryGlobal}, nil
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (suite *WalletTestSuite) TestCreateWallet() {
	setup := []types.MsgCreateWallet{
		{
			Creator: suite.addr1.String(),
			Name:    app.WalletName,
		},
		{
			Creator: suite.addr2.String(),
			Name:    app.WalletName,
		},
	}

	for _, element := range setup {
		oldLength := len(suite.keeper.GetAllWallet(suite.ctx))
		id := suite.keeper.AppendWallet(suite.ctx, element)

		exists := suite.keeper.HasWallet(suite.ctx, id)
		out := suite.keeper.GetWallet(suite.ctx, id)
		suite.Require().True(exists)
		suite.Require().Equal(element.Creator, out.Creator)
		suite.Require().Equal(app.WalletName, out.Name)
		suite.Require().Equal(oldLength+1, len(suite.keeper.GetAllWallet(suite.ctx)))

		// get wallet owner
		owner := suite.keeper.GetWalletOwner(suite.ctx, id)
		suite.Require().Equal(element.Creator, owner)
	}
}

func (suite *WalletTestSuite) TestCreateWalletWithId() {
	setup := []types.MsgCreateWalletWithId{
		{
			Creator: suite.addr1.String(),
			Id:      app.WalletId1,
			Name:    app.WalletName,
		},
		{
			Creator: suite.addr2.String(),
			Id:      app.WalletId2,
			Name:    app.WalletName,
		},
	}

	for _, element := range setup {
		oldLength := len(suite.keeper.GetAllWallet(suite.ctx))
		id := suite.keeper.AppendWalletWithId(suite.ctx, element)

		exists := suite.keeper.HasWallet(suite.ctx, id)
		out := suite.keeper.GetWallet(suite.ctx, id)
		suite.Require().True(exists)
		suite.Require().Equal(element.Creator, out.Creator)
		suite.Require().Equal(element.Id, out.Id)
		suite.Require().Equal(app.WalletName, out.Name)
		suite.Require().Equal(oldLength+1, len(suite.keeper.GetAllWallet(suite.ctx)))

		// get wallet owner
		owner := suite.keeper.GetWalletOwner(suite.ctx, id)
		suite.Require().Equal(element.Creator, owner)
	}
}

func (suite *WalletTestSuite) TestSetWallet() {
	setup := []string{
		"Wallet Name 1",
		"Wallet Name 2",
		"Wallet Name 3",
	}

	// create initial wallet
	wallet := types.MsgCreateWallet{
		Creator: suite.addr1.String(),
		Name:    app.WalletName,
	}
	id := suite.keeper.AppendWallet(suite.ctx, wallet)

	// get initial wallet
	out := suite.keeper.GetWallet(suite.ctx, id)
	b := suite.keeper.HasWalletHistory(suite.ctx, id)
	// suite.Require().False(b)

	for _, element := range setup {
		// set wallet
		out.Name = element
		suite.keeper.SetWallet(suite.ctx, out)
		b = suite.keeper.HasWalletHistory(suite.ctx, id)
		suite.Require().True(b)
	}
}

func (suite *WalletTestSuite) TestCreateSegment() {
	setup := []types.MsgCreateSegment{
		{
			Creator:  suite.addr1.String(),
			Name:     app.SegmentName,
			Info:     app.SegmentInfo,
			WalletId: app.WalletId1,
		},
		{
			Creator:  suite.addr2.String(),
			Name:     app.SegmentName,
			Info:     app.SegmentInfo,
			WalletId: app.WalletId2,
		},
	}

	for _, element := range setup {
		oldLength := len(suite.keeper.GetAllSegment(suite.ctx))
		id := suite.keeper.AppendSegment(suite.ctx, element)
		exists := suite.keeper.HasSegment(suite.ctx, id)
		suite.Require().True(exists)
		out := suite.keeper.GetSegment(suite.ctx, id)
		suite.Require().Equal(oldLength+1, len(suite.keeper.GetAllSegment(suite.ctx)))
		suite.Require().Equal(element.Creator, out.Creator)
		suite.Require().Equal(element.Name, out.Name)
		suite.Require().Equal(element.Info, out.Info)
		suite.Require().Equal(element.WalletId, out.WalletId)
	}
}

func (suite *WalletTestSuite) TestCreateSegmentWithId() {
	setup := []types.MsgCreateSegmentWithId{
		{
			Creator:  suite.addr1.String(),
			Id:       app.SegmentId1,
			Name:     app.SegmentName,
			Info:     app.SegmentInfo,
			WalletId: app.WalletId1,
		},
		{
			Creator:  suite.addr2.String(),
			Id:       app.SegmentId2,
			Name:     app.SegmentName,
			Info:     app.SegmentInfo,
			WalletId: app.WalletId2,
		},
	}

	for _, element := range setup {
		oldLength := len(suite.keeper.GetAllSegment(suite.ctx))
		id := suite.keeper.AppendSegmentWithId(suite.ctx, element)
		exists := suite.keeper.HasSegment(suite.ctx, id)
		suite.Require().True(exists)
		out := suite.keeper.GetSegment(suite.ctx, id)
		suite.Require().Equal(oldLength+1, len(suite.keeper.GetAllSegment(suite.ctx)))
		suite.Require().Equal(element.Creator, out.Creator)
		suite.Require().Equal(element.Id, out.Id)
		suite.Require().Equal(element.Name, out.Name)
		suite.Require().Equal(element.Info, out.Info)
		suite.Require().Equal(element.WalletId, out.WalletId)
	}
}

func (suite *WalletTestSuite) TestCreateWalletHistory() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	walletIds := []string{app.WalletId1, app.WalletId2}
	setup := []types.MsgCreateWalletHistory{
		{
			Creator: suite.addr1.String(),
			Id:      app.WalletId1,
			History: []*types.Wallet{},
		},
		{
			Creator: suite.addr2.String(),
			Id:      app.WalletId2,
			History: []*types.Wallet{},
		},
	}

	for index, element := range setup {
		// create history object for elem
		id := suite.keeper.AppendWalletHistory(suite.ctx, element)
		out := suite.keeper.GetWalletHistory(suite.ctx, id)

		// assert len / creator / id
		suite.Require().Equal(int(index)+2, len(suite.keeper.GetAllWalletHistory(suite.ctx)))
		suite.Require().Equal(addr[index], out.Creator)
		suite.Require().Equal(walletIds[index], out.Id)

		// get wallet history owner
		owner := suite.keeper.GetWalletHistoryOwner(suite.ctx, id)
		suite.Require().Equal(element.Creator, owner)
	}
}

func (suite *WalletTestSuite) TestSetSegment() {
	setup := []types.MsgUpdateSegment{
		{
			Creator: suite.addr1.String(),
			Name:    app.SegmentName,
			Info:    app.SegmentInfo,
		},
		{
			Creator: suite.addr2.String(),
			Name:    "Example Segment Name 2",
			Info:    "Example Segment Info 2",
		},
	}

	_, segmentId := suite.SetupWalletAndSegment()

	for _, element := range setup {
		segment := suite.keeper.GetSegment(suite.ctx, segmentId)
		segment.Name = element.Name
		segment.Info = element.Info
		segment.Creator = element.Creator
		// set segment
		suite.keeper.SetSegment(suite.ctx, segment)
		b := suite.keeper.HasSegmentHistory(suite.ctx, segmentId)
		suite.Require().True(b)

		// get segment owner
		owner := suite.keeper.GetSegmentOwner(suite.ctx, segmentId)
		suite.Require().Equal(element.Creator, owner)
	}
}

func (suite *WalletTestSuite) TestCreateSegmentHistory() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	segmentIds := []string{app.SegmentId1, app.SegmentId2}
	setup := []types.MsgCreateSegmentHistory{
		{
			Creator: suite.addr1.String(),
			Id:      app.SegmentId1,
			History: []*types.Segment{},
		},
		{
			Creator: suite.addr2.String(),
			Id:      app.SegmentId2,
			History: []*types.Segment{},
		},
	}

	for index, element := range setup {
		id := suite.keeper.AppendSegmentHistory(suite.ctx, element)
		out := suite.keeper.GetSegmentHistory(suite.ctx, id)
		suite.Require().Equal(int(index)+2, len(suite.keeper.GetAllSegmentHistory(suite.ctx)))
		suite.Require().Equal(addr[index], out.Creator)
		suite.Require().Equal(segmentIds[index], out.Id)

		// get segment history owner
		owner := suite.keeper.GetSegmentHistoryOwner(suite.ctx, id)
		suite.Require().Equal(element.Creator, owner)
	}
}

func (suite *WalletTestSuite) TestCreateTokenHistoryGlobal() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setup := []types.MsgCreateTokenHistoryGlobal{
		{
			Creator:   suite.addr1.String(),
			TokenId:   app.TokenId1,
			WalletIds: []string{},
		},
		{
			Creator:   suite.addr2.String(),
			TokenId:   app.TokenId2,
			WalletIds: []string{},
		},
	}

	for index, element := range setup {
		id := suite.keeper.AppendTokenHistoryGlobal(suite.ctx, element)
		exists := suite.keeper.HasTokenHistoryGlobal(suite.ctx, id)
		suite.Require().True(exists)

		out := suite.keeper.GetTokenHistoryGlobal(suite.ctx, id)
		suite.Require().Equal(int(index)+1, len(suite.keeper.GetAllTokenHistoryGlobal(suite.ctx)))
		suite.Require().Equal(addr[index], out.Creator)
		suite.Require().Equal(element.TokenId, out.Id)

		// get token history global owner
		owner := suite.keeper.GetTokenHistoryGlobalOwner(suite.ctx, id)
		suite.Require().Equal(element.Creator, owner)
	}
}

func (suite *WalletTestSuite) TestSetTokenHistoryGlobal() {
	// create token history global entry
	id := suite.keeper.AppendTokenHistoryGlobal(suite.ctx, types.MsgCreateTokenHistoryGlobal{
		Creator: suite.addr1.String(),
		TokenId: app.TokenId1,
	})

	out := suite.keeper.GetTokenHistoryGlobal(suite.ctx, id)
	out.WalletId = append(out.WalletId, app.WalletId1)

	suite.keeper.SetTokenHistoryGlobal(suite.ctx, out)
	out = suite.keeper.GetTokenHistoryGlobal(suite.ctx, id)
	out = suite.keeper.GetTokenHistoryGlobal(suite.ctx, id)
}

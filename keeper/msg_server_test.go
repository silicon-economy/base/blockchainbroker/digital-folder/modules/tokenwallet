// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (suite *WalletTestSuite) TestMsgCreateWallet() {
	tests := []struct {
		name    string
		msg     *types.MsgCreateWallet
		wantErr bool
	}{
		{
			name: app.SuccessfulTx,
			msg: &types.MsgCreateWallet{
				Creator: app.CreatorA,
				Name:    app.WalletName,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		id, errCreate := suite.msgServer.CreateWallet(suite.goCtx, tt.msg)
		got := suite.keeper.GetWallet(suite.ctx, id.Id)

		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(app.WalletName, got.Name)
		}
	}
}

func (suite *WalletTestSuite) TestMsgCreateWalletWithId() {
	tests := []struct {
		name    string
		msg     *types.MsgCreateWalletWithId
		wantErr bool
	}{
		{
			name: app.SuccessfulTx,
			msg: &types.MsgCreateWalletWithId{
				Creator: app.CreatorA,
				Id:      app.WalletId1,
				Name:    app.WalletName,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		id, errCreate := suite.msgServer.CreateWalletWithId(suite.goCtx, tt.msg)
		got := suite.keeper.GetWallet(suite.ctx, id.Id)

		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(app.WalletName, got.Name)
		}
	}
}

func (suite *WalletTestSuite) TestMsgUpdateWallet() {
	walletId := suite.keeper.AppendWallet(suite.ctx, types.MsgCreateWallet{
		Creator: app.CreatorA,
		Name:    app.WalletName,
	})
	newWalletName := "NEW_WALLET_NAME"

	tests := []struct {
		name    string
		msg     *types.MsgUpdateWallet
		wantErr bool
	}{
		{
			name: app.SuccessfulTx,
			msg: &types.MsgUpdateWallet{
				Creator: app.CreatorA,
				Id:      walletId,
				Name:    newWalletName,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		_, errCreate := suite.msgServer.UpdateWallet(suite.goCtx, tt.msg)
		got := suite.keeper.GetWallet(suite.ctx, walletId)

		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(newWalletName, got.Name)
		}
	}
}

func (suite *WalletTestSuite) TestMsgCreateSegment() {
	walletId := suite.keeper.AppendWallet(suite.ctx, types.MsgCreateWallet{
		Creator: app.CreatorA,
		Name:    app.WalletName,
	})

	tests := []struct {
		name    string
		msg     *types.MsgCreateSegment
		wantErr bool
	}{
		{
			name: app.SuccessfulTx,
			msg: &types.MsgCreateSegment{
				Creator:  app.CreatorA,
				WalletId: walletId,
				Name:     app.SegmentName,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		oldLen := len(suite.keeper.GetWallet(suite.ctx, walletId).SegmentIds)
		id, errCreate := suite.msgServer.CreateSegment(suite.goCtx, tt.msg)
		got := suite.keeper.GetWallet(suite.ctx, walletId)

		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(oldLen+1, len(got.SegmentIds))
			seg := suite.keeper.GetSegment(suite.ctx, id.Id)
			suite.Require().Equal(app.SegmentName, seg.Name)
		}
	}
}
func (suite *WalletTestSuite) TestMsgCreateSegmentWithId() {
	walletId := suite.keeper.AppendWallet(suite.ctx, types.MsgCreateWallet{
		Creator: app.CreatorA,
		Name:    app.WalletName,
	})

	tests := []struct {
		name    string
		msg     *types.MsgCreateSegmentWithId
		wantErr bool
	}{
		{
			name: app.SuccessfulTx,
			msg: &types.MsgCreateSegmentWithId{
				Creator:  app.CreatorA,
				Id:       app.SegmentId1,
				Name:     app.SegmentName,
				WalletId: walletId,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		oldLen := len(suite.keeper.GetWallet(suite.ctx, walletId).SegmentIds)
		id, errCreate := suite.msgServer.CreateSegmentWithId(suite.goCtx, tt.msg)
		got := suite.keeper.GetWallet(suite.ctx, walletId)

		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(oldLen+1, len(got.SegmentIds))
			seg := suite.keeper.GetSegment(suite.ctx, id.Id)
			suite.Require().Equal(app.SegmentName, seg.Name)
		}
	}
}

func (suite *WalletTestSuite) TestMsgUpdateSegment() {
	walletId := suite.keeper.AppendWallet(suite.ctx, types.MsgCreateWallet{
		Creator: app.CreatorA,
		Name:    app.WalletName,
	})
	segment, _ := suite.msgServer.CreateSegment(suite.goCtx, &types.MsgCreateSegment{
		Creator:  app.CreatorA,
		WalletId: walletId,
		Name:     app.SegmentName,
	})

	newSegmentName := "NEW_SEGMENT_NAME"

	tests := []struct {
		name    string
		msg     *types.MsgUpdateSegment
		wantErr bool
	}{
		{
			name: app.SuccessfulTx,
			msg: &types.MsgUpdateSegment{
				Creator: app.CreatorA,
				Id:      segment.Id,
				Name:    newSegmentName,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		_, errCreate := suite.msgServer.UpdateSegment(suite.goCtx, tt.msg)
		got := suite.keeper.GetSegment(suite.ctx, segment.Id)

		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(newSegmentName, got.Name)
		}
	}
}

func (suite *WalletTestSuite) TestMsgAssignCosmosAddressToWallet() {
	walletId := suite.keeper.AppendWallet(suite.ctx, types.MsgCreateWallet{
		Creator: app.CreatorA,
		Name:    app.WalletName,
	})

	tests := []struct {
		name    string
		msg     *types.MsgAssignCosmosAddressToWallet
		wantErr bool
	}{
		{
			name: app.SuccessfulTx,
			msg: &types.MsgAssignCosmosAddressToWallet{
				Creator:       app.CreatorA,
				WalletId:      walletId,
				CosmosAddress: app.CreatorA,
			},
			wantErr: false,
		},
		{
			name: app.WalletNotFound,
			msg: &types.MsgAssignCosmosAddressToWallet{
				Creator:       app.CreatorB,
				WalletId:      app.WalletId2,
				CosmosAddress: app.CreatorA,
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		oldLen := len(suite.keeper.GetWallet(suite.ctx, walletId).WalletAccounts)
		_, errCreate := suite.msgServer.AssignCosmosAddressToWallet(suite.goCtx, tt.msg)
		got := suite.keeper.GetWallet(suite.ctx, walletId)

		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(oldLen+1, len(got.WalletAccounts))
		}
	}
}

func (suite *WalletTestSuite) TestMsgCreateTokenRef() {
	walletId := suite.keeper.AppendWallet(suite.ctx, types.MsgCreateWallet{
		Creator: app.CreatorA,
		Name:    app.WalletName,
	})
	segment, _ := suite.msgServer.CreateSegment(suite.goCtx, &types.MsgCreateSegment{
		Creator:  app.CreatorA,
		WalletId: walletId,
		Name:     app.SegmentName,
	})

	tokenId := "TOKEN_ID"
	moduleRef := "MODULE_REF"

	tests := []struct {
		name    string
		msg     *types.MsgCreateTokenRef
		wantErr bool
	}{
		{
			name: app.SuccessfulTx,
			msg: &types.MsgCreateTokenRef{
				Creator:   app.CreatorA,
				Id:        tokenId,
				SegmentId: segment.Id,
				ModuleRef: moduleRef,
			},
			wantErr: false,
		},
		{
			name: app.SegmentNotFound,
			msg: &types.MsgCreateTokenRef{
				Creator:   app.CreatorB,
				Id:        tokenId,
				SegmentId: app.SegmentId2,
				ModuleRef: moduleRef,
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		oldLen := len(suite.keeper.GetSegment(suite.ctx, segment.Id).TokenRefs)
		_, errCreate := suite.msgServer.CreateTokenRef(suite.goCtx, tt.msg)
		got := suite.keeper.GetSegment(suite.ctx, segment.Id)

		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(oldLen+1, len(got.TokenRefs))
		}
	}
}

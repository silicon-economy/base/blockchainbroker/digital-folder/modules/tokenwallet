// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"reflect"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func TestFetchTokenHistoryGlobal(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchGetTokenHistoryGlobal
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchGetTokenHistoryGlobalResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchGetTokenHistoryGlobal{Creator: app.CreatorA, Id: app.TokenId1}},
			want:    &types.MsgFetchGetTokenHistoryGlobalResponse{TokenHistoryGlobal: &types.TokenHistoryGlobal{Id: app.TokenId1}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			newKeeper.SetTokenHistoryGlobal(ctx, types.TokenHistoryGlobal{Id: app.TokenId1})

			got, err := msgServer.FetchTokenHistoryGlobal(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchTokenHistoryGlobal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchTokenHistoryGlobal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFetchTokenHistoryGlobalAll(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchAllTokenHistoryGlobal
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchAllTokenHistoryGlobalResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchAllTokenHistoryGlobal{Creator: app.CreatorA}},
			want:    &types.MsgFetchAllTokenHistoryGlobalResponse{TokenHistoryGlobal: []*types.TokenHistoryGlobal{{Id: app.TokenId1}}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			newKeeper.SetTokenHistoryGlobal(ctx, types.TokenHistoryGlobal{Id: app.TokenId1})

			got, err := msgServer.FetchTokenHistoryGlobalAll(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchTokenHistoryGlobalAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchTokenHistoryGlobalAll() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFetchSegmentHistory(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchGetSegmentHistory
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchGetSegmentHistoryResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchGetSegmentHistory{Creator: app.CreatorA, Id: app.SegmentId1}},
			want:    &types.MsgFetchGetSegmentHistoryResponse{SegmentHistory: &types.SegmentHistory{Id: app.SegmentId1}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			newKeeper.SetSegmentHistory(ctx, types.SegmentHistory{Id: app.SegmentId1})

			got, err := msgServer.FetchSegmentHistory(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchSegmentHistory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchSegmentHistory() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFetchSegmentHistoryAll(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchAllSegmentHistory
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchAllSegmentHistoryResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchAllSegmentHistory{Creator: app.CreatorA}},
			want:    &types.MsgFetchAllSegmentHistoryResponse{SegmentHistory: []*types.SegmentHistory{{Id: app.SegmentId1}}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			newKeeper.SetSegmentHistory(ctx, types.SegmentHistory{Id: app.SegmentId1})

			got, err := msgServer.FetchSegmentHistoryAll(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchSegmentHistoryAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchSegmentHistoryAll() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFetchSegment(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchGetSegment
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchGetSegmentResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchGetSegment{Creator: app.CreatorA, Id: app.SegmentId1}},
			want:    &types.MsgFetchGetSegmentResponse{Segment: &types.Segment{Id: app.SegmentId1}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			newKeeper.SetSegment(ctx, types.Segment{Id: app.SegmentId1})

			got, err := msgServer.FetchSegment(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchSegment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchSegment() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFetchSegmentAll(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchAllSegment
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchAllSegmentResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchAllSegment{Creator: app.CreatorA}},
			want:    &types.MsgFetchAllSegmentResponse{Segment: []*types.Segment{{Id: app.SegmentId1}}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			newKeeper.SetSegment(ctx, types.Segment{Id: app.SegmentId1})

			got, err := msgServer.FetchSegmentAll(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchSegmentAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchSegmentAll() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFetchWalletHistory(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchGetWalletHistory
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchGetWalletHistoryResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchGetWalletHistory{Creator: app.CreatorA, Id: app.WalletId1}},
			want:    &types.MsgFetchGetWalletHistoryResponse{WalletHistory: &types.WalletHistory{Id: app.WalletId1}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			newKeeper.SetWalletHistory(ctx, types.WalletHistory{Id: app.WalletId1})

			got, err := msgServer.FetchWalletHistory(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchWalletHistory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchWalletHistory() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFetchWalletHistoryAll(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchAllWalletHistory
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchAllWalletHistoryResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchAllWalletHistory{Creator: app.CreatorA}},
			want:    &types.MsgFetchAllWalletHistoryResponse{WalletHistory: []*types.WalletHistory{{Id: app.WalletId1}}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			newKeeper.SetWalletHistory(ctx, types.WalletHistory{Id: app.WalletId1})

			got, err := msgServer.FetchWalletHistoryAll(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchWalletHistory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchWalletHistory() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFetchWallet(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchGetWallet
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchGetWalletResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchGetWallet{Creator: app.CreatorA, Id: app.WalletId1}},
			want:    &types.MsgFetchGetWalletResponse{Wallet: &types.Wallet{Id: app.WalletId1}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			newKeeper.SetWallet(ctx, types.Wallet{Id: app.WalletId1})

			got, err := msgServer.FetchWallet(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchWallet() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchWallet() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFetchWalletAll(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchAllWallet
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchAllWalletResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchAllWallet{Creator: app.CreatorA}},
			want:    &types.MsgFetchAllWalletResponse{Wallet: []*types.Wallet{{Id: app.WalletId1}}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			newKeeper.SetWallet(ctx, types.Wallet{Id: app.WalletId1})

			got, err := msgServer.FetchWalletAll(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchWalletAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchWalletAll() = %v, want %v", got, tt.want)
			}
		})
	}
}

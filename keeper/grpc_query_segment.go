// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k Keeper) SegmentAll(c context.Context, req *types.QueryAllSegmentRequest) (*types.QueryAllSegmentResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	var segments []*types.Segment
	store := ctx.KVStore(k.storeKey)
	segmentStore := prefix.NewStore(store, types.KeyPrefix(types.SegmentKey))

	pageRes, err := query.Paginate(segmentStore, req.Pagination, func(key []byte, value []byte) error {
		var segment types.Segment
		if err := k.cdc.Unmarshal(value, &segment); err != nil {
			return err
		}

		segments = append(segments, &segment)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllSegmentResponse{Segment: segments, Pagination: pageRes}, nil
}

func (k Keeper) Segment(c context.Context, req *types.QueryGetSegmentRequest) (*types.QueryGetSegmentResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)

	var segment types.Segment
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentKey))

	if !store.Has(types.KeyPrefix(types.SegmentKey + req.Id)) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.SegmentKey+req.Id)), &segment)

	return &types.QueryGetSegmentResponse{Segment: &segment}, nil
}

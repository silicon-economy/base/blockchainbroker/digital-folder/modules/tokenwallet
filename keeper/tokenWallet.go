// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

// GetWalletCount gets the total number of wallet
func (k Keeper) GetWalletCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletCountKey))
	byteKey := types.KeyPrefix(types.WalletCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetWalletCount sets the total number of wallet
func (k Keeper) SetWalletCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletCountKey))
	byteKey := types.KeyPrefix(types.WalletCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendWallet appends a wallet in the store with a new id and updates the count
func (k Keeper) AppendWallet(ctx sdk.Context, msg types.MsgCreateWallet) string {
	// Create the wallet
	count := k.GetWalletCount(ctx)

	// Create wallet id
	id := strconv.FormatInt(count, 10)

	var wallet = types.Wallet{
		Creator:   msg.Creator,
		Id:        id,
		Name:      msg.Name,
		Timestamp: GetTimestamp(),
	}

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletKey))
	key := types.KeyPrefix(types.WalletKey + wallet.Id)
	value := k.cdc.MustMarshal(&wallet)
	store.Set(key, value)

	// Update wallet count
	k.SetWalletCount(ctx, count+1)

	k.AppendSegment(ctx, types.MsgCreateSegment{Creator: msg.Creator, Name: types.Inbox, WalletId: id, Info: types.InboxInfo})
	return id
}

// AppendWalletWithId appends a wallet in the store with a new id and updates the count
func (k Keeper) AppendWalletWithId(ctx sdk.Context, msg types.MsgCreateWalletWithId) string {
	// Create the wallet
	count := k.GetWalletCount(ctx)
	// Create wallet id
	id := msg.Id

	var wallet = types.Wallet{
		Creator:   msg.Creator,
		Id:        id,
		Name:      msg.Name,
		Timestamp: GetTimestamp(),
	}

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletKey))
	key := types.KeyPrefix(types.WalletKey + wallet.Id)
	value := k.cdc.MustMarshal(&wallet)
	store.Set(key, value)

	// Update wallet count
	k.SetWalletCount(ctx, count+1)

	k.AppendSegment(ctx, types.MsgCreateSegment{Creator: msg.Creator, Name: types.Inbox, WalletId: id, Info: types.InboxInfo})
	return id
}

// SetWallet sets a specific wallet in the store
func (k Keeper) SetWallet(ctx sdk.Context, wallet types.Wallet) {
	oldWallet := k.GetWallet(ctx, wallet.Id)

	k.CreateWalletHistoryEntry(ctx, oldWallet)

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletKey))
	b := k.cdc.MustMarshal(&wallet)
	store.Set(types.KeyPrefix(types.WalletKey+wallet.Id), b)
}

// CreateWalletHistoryEntry creates an entry in the walletHistory
func (k Keeper) CreateWalletHistoryEntry(ctx sdk.Context, oldWallet types.Wallet) {
	walletHistoryExists := k.HasWalletHistory(ctx, oldWallet.Id)

	if walletHistoryExists {
		walletHistory := k.GetWalletHistory(ctx, oldWallet.Id)
		updatedHistory := append(walletHistory.History, &oldWallet)
		walletHistory.History = updatedHistory
		k.SetWalletHistory(ctx, walletHistory)
	} else {
		walletHistory := types.MsgCreateWalletHistory{
			Creator: oldWallet.Creator,
			Id:      oldWallet.Id,
			History: []*types.Wallet{
				&oldWallet,
			},
		}
		k.AppendWalletHistory(ctx, walletHistory)
	}
}

// AppendWalletSegmentIds appends a segmentId to a wallet
func (k Keeper) AppendWalletSegmentIds(ctx sdk.Context, walletId string, segmentId string) {
	// wallet Param contains the new SegmentIds but the basic attributes should be retrieved from the old state
	wallet := k.GetWallet(ctx, walletId)
	wallet.SegmentIds = append(wallet.SegmentIds, segmentId)
	k.SetWallet(ctx, wallet)
}

// GetWallet returns a wallet from its id
func (k Keeper) GetWallet(ctx sdk.Context, key string) types.Wallet {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletKey))
	var wallet types.Wallet
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.WalletKey+key)), &wallet)
	return wallet
}

// HasWallet checks if the wallet exists in the store
func (k Keeper) HasWallet(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletKey))
	return store.Has(types.KeyPrefix(types.WalletKey + id))
}

// GetWalletOwner returns the creator of the wallet
func (k Keeper) GetWalletOwner(ctx sdk.Context, key string) string {
	return k.GetWallet(ctx, key).Creator
}

// GetAllWallet returns all wallet
func (k Keeper) GetAllWallet(ctx sdk.Context) (msgs []types.Wallet) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.WalletKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.Wallet
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"
	"time"

	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/tendermint/tendermint/libs/log"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

type (
	Keeper struct {
		cdc      codec.Codec
		storeKey sdk.StoreKey
		memKey   sdk.StoreKey
	}
)

func NewKeeper(cdc codec.Codec, storeKey, memKey sdk.StoreKey) *Keeper {
	return &Keeper{
		cdc:      cdc,
		storeKey: storeKey,
		memKey:   memKey,
	}
}

func (k Keeper) Logger(ctx sdk.Context) log.Logger {
	return ctx.Logger().With("module", fmt.Sprintf("x/%s", types.ModuleName))
}

func GetTimestamp() string {
	return time.Now().UTC().Format(time.RFC3339)
}

// InitGenesis initializes the capability module's state from a provided genesis state.
func (k Keeper) InitGenesis(ctx sdk.Context, genState types.GenesisState) {
	// this line is used by starport scaffolding # genesis/module/init
	// Set all the tokenHistoryGlobal
	for _, elem := range genState.TokenHistoryGlobalList {
		k.SetTokenHistoryGlobal(ctx, *elem)
	}

	// Set tokenHistoryGlobal count
	k.SetTokenHistoryGlobalCount(ctx, int64(len(genState.TokenHistoryGlobalList)))

	// Set all the segmentHistory
	for _, elem := range genState.SegmentHistoryList {
		k.SetSegmentHistory(ctx, *elem)
	}

	// Set segmentHistory count
	k.SetSegmentHistoryCount(ctx, int64(len(genState.SegmentHistoryList)))

	// Set all the walletHistory
	for _, elem := range genState.WalletHistoryList {
		k.SetWalletHistory(ctx, *elem)
	}

	// Set walletHistory count
	k.SetWalletHistoryCount(ctx, int64(len(genState.WalletHistoryList)))

	// Set all the segment
	for _, elem := range genState.SegmentList {
		k.SetSegment(ctx, *elem)
	}

	// Set segment count
	k.SetSegmentCount(ctx, int64(len(genState.SegmentList)))

	// Set all the wallet
	for _, elem := range genState.WalletList {
		k.SetWallet(ctx, *elem)
	}

	// Set wallet count
	k.SetWalletCount(ctx, int64(len(genState.WalletList)))
}

// RevertToGenesis reverts to genesis state
func (k Keeper) RevertToGenesis(ctx sdk.Context) {
	kvStore := ctx.KVStore(k.storeKey)
	it := kvStore.Iterator(nil, nil)
	defer it.Close()

	for ; it.Valid(); it.Next() {
		kvStore.Delete(it.Key())
	}

	k.InitGenesis(ctx, *types.DefaultGenesis())
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

// GetTokenHistoryGlobalCount gets the total number of tokenHistoryGlobal
func (k Keeper) GetTokenHistoryGlobalCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryGlobalCountKey))
	byteKey := types.KeyPrefix(types.TokenHistoryGlobalCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetTokenHistoryGlobalCount sets the total number of tokenHistoryGlobal
func (k Keeper) SetTokenHistoryGlobalCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryGlobalCountKey))
	byteKey := types.KeyPrefix(types.TokenHistoryGlobalCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendTokenHistoryGlobal appends a tokenHistoryGlobal in the store with a new id and updates the count
func (k Keeper) AppendTokenHistoryGlobal(ctx sdk.Context, msg types.MsgCreateTokenHistoryGlobal) string {
	// Create the tokenHistoryGlobal
	count := k.GetTokenHistoryGlobalCount(ctx)
	var tokenHistoryGlobal = types.TokenHistoryGlobal{
		Creator: msg.Creator,
		Id:      msg.TokenId,
	}

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryGlobalKey))
	key := types.KeyPrefix(types.TokenHistoryGlobalKey + tokenHistoryGlobal.Id)
	value := k.cdc.MustMarshal(&tokenHistoryGlobal)
	store.Set(key, value)

	// Update tokenHistoryGlobal count
	k.SetTokenHistoryGlobalCount(ctx, count+1)

	return msg.TokenId
}

// SetTokenHistoryGlobal sets a specific tokenHistoryGlobal in the store
func (k Keeper) SetTokenHistoryGlobal(ctx sdk.Context, tokenHistoryGlobal types.TokenHistoryGlobal) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryGlobalKey))
	b := k.cdc.MustMarshal(&tokenHistoryGlobal)
	store.Set(types.KeyPrefix(types.TokenHistoryGlobalKey+tokenHistoryGlobal.Id), b)
}

// GetTokenHistoryGlobal returns a tokenHistoryGlobal from its id
func (k Keeper) GetTokenHistoryGlobal(ctx sdk.Context, key string) types.TokenHistoryGlobal {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryGlobalKey))
	var tokenHistoryGlobal types.TokenHistoryGlobal
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.TokenHistoryGlobalKey+key)), &tokenHistoryGlobal)
	return tokenHistoryGlobal
}

// HasTokenHistoryGlobal checks if the tokenHistoryGlobal exists in the store
func (k Keeper) HasTokenHistoryGlobal(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryGlobalKey))
	return store.Has(types.KeyPrefix(types.TokenHistoryGlobalKey + id))
}

// GetTokenHistoryGlobalOwner returns the creator of the tokenHistoryGlobal
func (k Keeper) GetTokenHistoryGlobalOwner(ctx sdk.Context, key string) string {
	return k.GetTokenHistoryGlobal(ctx, key).Creator
}

// GetAllTokenHistoryGlobal returns all tokenHistoryGlobal
func (k Keeper) GetAllTokenHistoryGlobal(ctx sdk.Context) (msgs []types.TokenHistoryGlobal) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryGlobalKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.TokenHistoryGlobalKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.TokenHistoryGlobal
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}

// CreateTokenHistoryGlobalEntry creates an entry in the tokenHistoryGlobal
func (k Keeper) CreateTokenHistoryGlobalEntry(ctx sdk.Context, creator string, tokenRefId string, tokenwalletId string) {
	if k.HasTokenHistoryGlobal(ctx, tokenRefId) {
		tokenHistoryGlobal := k.GetTokenHistoryGlobal(ctx, tokenRefId)
		tokenHistoryGlobal.WalletId = append(tokenHistoryGlobal.WalletId, tokenwalletId)
		k.SetTokenHistoryGlobal(ctx, tokenHistoryGlobal)
	} else {
		tokenHistoryGlobal := types.MsgCreateTokenHistoryGlobal{
			Creator:   creator,
			TokenId:   tokenRefId,
			WalletIds: []string{tokenwalletId},
		}
		k.AppendTokenHistoryGlobal(ctx, tokenHistoryGlobal)
	}
}

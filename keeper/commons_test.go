// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"testing"

	"github.com/cosmos/cosmos-sdk/baseapp"
	"github.com/cosmos/cosmos-sdk/codec"
	codecTypes "github.com/cosmos/cosmos-sdk/codec/types"
	"github.com/cosmos/cosmos-sdk/store"
	storeTypes "github.com/cosmos/cosmos-sdk/store/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tendermint/tendermint/libs/log"
	tendermintTypes "github.com/tendermint/tendermint/proto/tendermint/types"
	tendermintTmDb "github.com/tendermint/tm-db"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"
	tokenwalletKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

type WalletTestSuite struct {
	suite.Suite
	keeper      tokenwalletKeeper.Keeper
	app         *app.App
	ctx         sdk.Context
	goCtx       context.Context
	queryClient types.QueryClient
	addr1       sdk.AccAddress
	addr2       sdk.AccAddress
	msgServer   types.MsgServer
}

func (suite *WalletTestSuite) SetupTest() {
	testableApp, ctx := app.CreateTestInput()

	suite.keeper = testableApp.WalletKeeper
	suite.app = testableApp
	suite.ctx = ctx
	suite.goCtx = sdk.WrapSDKContext(suite.ctx)

	creator1, _ := sdk.AccAddressFromBech32(app.CreatorA)
	creator2, _ := sdk.AccAddressFromBech32(app.CreatorB)

	suite.addr1 = creator1
	suite.addr2 = creator2

	querier := keeper.Querier{Keeper: testableApp.WalletKeeper}
	queryHelper := baseapp.NewQueryServerTestHelper(ctx, testableApp.InterfaceRegistry())
	types.RegisterQueryServer(queryHelper, querier)

	suite.queryClient = types.NewQueryClient(queryHelper)
	suite.msgServer = keeper.NewMsgServerImpl(suite.keeper)
}

func (suite *WalletTestSuite) SetupWalletAndSegment() (string, string) {
	// initial wallet
	msgCreateWallet := types.MsgCreateWallet{
		Creator: suite.addr1.String(),
		Name:    app.WalletName,
	}
	walletId := suite.keeper.AppendWallet(suite.ctx, msgCreateWallet)

	// initial segment
	msgCreateSegment := types.MsgCreateSegment{
		Creator:  suite.addr1.String(),
		Name:     app.SegmentName,
		Info:     app.SegmentInfo,
		WalletId: walletId,
	}

	segmentId := suite.keeper.AppendSegment(suite.ctx, msgCreateSegment)

	return walletId, segmentId
}

func (suite *WalletTestSuite) SetupTokenHistoryGlobal() string {
	// initial wallet
	msgCreateWallet := types.MsgCreateWallet{
		Creator: suite.addr1.String(),
		Name:    app.WalletName,
	}
	walletId := suite.keeper.AppendWallet(suite.ctx, msgCreateWallet)

	// initial segment
	msgCreateSegment := types.MsgCreateSegment{
		Creator:  suite.addr1.String(),
		Name:     app.SegmentName,
		Info:     app.SegmentInfo,
		WalletId: walletId,
	}

	suite.keeper.AppendSegment(suite.ctx, msgCreateSegment)

	msgTokenHistoryGlobal := types.MsgCreateTokenHistoryGlobal{
		Creator:   suite.addr1.String(),
		TokenId:   app.TokenId1,
		WalletIds: []string{walletId},
	}

	id := suite.keeper.AppendTokenHistoryGlobal(suite.ctx, msgTokenHistoryGlobal)

	return id
}

func TestWalletTestSuite(t *testing.T) {
	suite.Run(t, new(WalletTestSuite))
}

func setupKeeper(t testing.TB) (*keeper.Keeper, sdk.Context) {
	storeKey := sdk.NewKVStoreKey(types.StoreKey)
	memStoreKey := storeTypes.NewMemoryStoreKey(types.MemStoreKey)

	db := tendermintTmDb.NewMemDB()
	stateStore := store.NewCommitMultiStore(db)
	stateStore.MountStoreWithDB(storeKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(memStoreKey, sdk.StoreTypeMemory, nil)
	require.NoError(t, stateStore.LoadLatestVersion())

	registry := codecTypes.NewInterfaceRegistry()
	newKeeper := keeper.NewKeeper(
		codec.NewProtoCodec(registry),
		storeKey,
		memStoreKey,
	)

	ctx := sdk.NewContext(stateStore, tendermintTypes.Header{}, false, log.NewNopLogger())

	return newKeeper, ctx
}

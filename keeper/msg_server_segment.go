// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k Keeper) CreateSegment(goCtx context.Context, msg *types.MsgCreateSegment) (*types.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	if !k.HasWallet(ctx, msg.WalletId) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.WalletId))
	}

	id := k.AppendSegment(ctx, *msg)

	return &types.MsgIdResponse{
		Id: id,
	}, nil
}

func (k Keeper) CreateSegmentWithId(goCtx context.Context, msg *types.MsgCreateSegmentWithId) (*types.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	if !k.HasWallet(ctx, msg.WalletId) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.WalletId))
	}

	id := k.AppendSegmentWithId(ctx, *msg)

	return &types.MsgIdResponse{
		Id: id,
	}, nil
}

func (k Keeper) UpdateSegment(goCtx context.Context, msg *types.MsgUpdateSegment) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	var oldSegment = k.GetSegment(ctx, msg.Id)
	var segment = types.Segment{
		Creator:   msg.Creator,
		Id:        msg.Id,
		Name:      msg.Name,
		Timestamp: GetTimestamp(),
		Info:      msg.Info,
		WalletId:  oldSegment.WalletId,
		TokenRefs: oldSegment.TokenRefs,
	}

	// Checks that the element exists
	if !k.HasSegment(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	// Checks if the msg sender is the same as the current owner
	if msg.Creator != k.GetSegmentOwner(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrUnauthorized, "incorrect owner")
	}

	k.SetSegment(ctx, segment)

	return &types.MsgEmptyResponse{}, nil
}

func (k Keeper) CreateTokenRef(goCtx context.Context, msg *types.MsgCreateTokenRef) (*types.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// No permission check here because it's an internally used function used by the businesslogic
	if !k.HasSegment(ctx, msg.SegmentId) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.SegmentId))
	}

	k.AppendTokenRef(ctx, *msg)

	return &types.MsgIdResponse{
		Id: msg.Id,
	}, nil
}

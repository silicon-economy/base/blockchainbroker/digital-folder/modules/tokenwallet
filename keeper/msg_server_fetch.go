// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k Keeper) FetchTokenHistoryGlobal(goCtx context.Context, msg *types.MsgFetchGetTokenHistoryGlobal) (*types.MsgFetchGetTokenHistoryGlobalResponse, error) {
	res, _ := k.TokenHistoryGlobal(goCtx, &types.QueryGetTokenHistoryGlobalRequest{Id: msg.Id})
	return &types.MsgFetchGetTokenHistoryGlobalResponse{TokenHistoryGlobal: res.TokenHistoryGlobal}, nil
}

func (k Keeper) FetchTokenHistoryGlobalAll(goCtx context.Context, msg *types.MsgFetchAllTokenHistoryGlobal) (*types.MsgFetchAllTokenHistoryGlobalResponse, error) {
	res, _ := k.TokenHistoryGlobalAll(goCtx, &types.QueryAllTokenHistoryGlobalRequest{Pagination: msg.Pagination})
	return &types.MsgFetchAllTokenHistoryGlobalResponse{TokenHistoryGlobal: res.TokenHistoryGlobal}, nil
}

func (k Keeper) FetchSegmentHistory(goCtx context.Context, msg *types.MsgFetchGetSegmentHistory) (*types.MsgFetchGetSegmentHistoryResponse, error) {
	res, _ := k.SegmentHistory(goCtx, &types.QueryGetSegmentHistoryRequest{Id: msg.Id})
	return &types.MsgFetchGetSegmentHistoryResponse{SegmentHistory: res.SegmentHistory}, nil
}

func (k Keeper) FetchSegmentHistoryAll(goCtx context.Context, msg *types.MsgFetchAllSegmentHistory) (*types.MsgFetchAllSegmentHistoryResponse, error) {
	res, _ := k.SegmentHistoryAll(goCtx, &types.QueryAllSegmentHistoryRequest{Pagination: msg.Pagination})
	return &types.MsgFetchAllSegmentHistoryResponse{SegmentHistory: res.SegmentHistory}, nil
}

func (k Keeper) FetchSegment(goCtx context.Context, msg *types.MsgFetchGetSegment) (*types.MsgFetchGetSegmentResponse, error) {
	res, _ := k.Segment(goCtx, &types.QueryGetSegmentRequest{Id: msg.Id})
	return &types.MsgFetchGetSegmentResponse{Segment: res.Segment}, nil
}

func (k Keeper) FetchSegmentAll(goCtx context.Context, msg *types.MsgFetchAllSegment) (*types.MsgFetchAllSegmentResponse, error) {
	res, _ := k.SegmentAll(goCtx, &types.QueryAllSegmentRequest{Pagination: msg.Pagination})
	return &types.MsgFetchAllSegmentResponse{Segment: res.Segment}, nil
}

func (k Keeper) FetchWalletHistory(goCtx context.Context, msg *types.MsgFetchGetWalletHistory) (*types.MsgFetchGetWalletHistoryResponse, error) {
	res, _ := k.WalletHistory(goCtx, &types.QueryGetWalletHistoryRequest{Id: msg.Id})
	return &types.MsgFetchGetWalletHistoryResponse{WalletHistory: res.WalletHistory}, nil
}

func (k Keeper) FetchWalletHistoryAll(goCtx context.Context, msg *types.MsgFetchAllWalletHistory) (*types.MsgFetchAllWalletHistoryResponse, error) {
	res, _ := k.WalletHistoryAll(goCtx, &types.QueryAllWalletHistoryRequest{Pagination: msg.Pagination})
	return &types.MsgFetchAllWalletHistoryResponse{WalletHistory: res.WalletHistory}, nil
}

func (k Keeper) FetchWallet(goCtx context.Context, msg *types.MsgFetchGetWallet) (*types.MsgFetchGetWalletResponse, error) {
	res, _ := k.Wallet(goCtx, &types.QueryGetWalletRequest{Id: msg.Id})
	return &types.MsgFetchGetWalletResponse{Wallet: res.Wallet}, nil
}

func (k Keeper) FetchWalletAll(goCtx context.Context, msg *types.MsgFetchAllWallet) (*types.MsgFetchAllWalletResponse, error) {
	res, _ := k.WalletAll(goCtx, &types.QueryAllWalletRequest{Pagination: msg.Pagination})
	return &types.MsgFetchAllWalletResponse{Wallet: res.Wallet}, nil
}

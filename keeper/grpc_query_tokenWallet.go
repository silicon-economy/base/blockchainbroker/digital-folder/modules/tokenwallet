// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k Keeper) WalletAll(c context.Context, req *types.QueryAllWalletRequest) (*types.QueryAllWalletResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	var wallets []*types.Wallet
	store := ctx.KVStore(k.storeKey)
	walletStore := prefix.NewStore(store, types.KeyPrefix(types.WalletKey))

	pageRes, err := query.Paginate(walletStore, req.Pagination, func(key []byte, value []byte) error {
		var wallet types.Wallet
		if err := k.cdc.Unmarshal(value, &wallet); err != nil {
			return err
		}

		wallets = append(wallets, &wallet)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllWalletResponse{Wallet: wallets, Pagination: pageRes}, nil
}

func (k Keeper) Wallet(c context.Context, req *types.QueryGetWalletRequest) (*types.QueryGetWalletResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)

	if !k.HasWallet(ctx, req.Id) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	wallet := k.GetWallet(ctx, req.Id)
	return &types.QueryGetWalletResponse{Wallet: &wallet}, nil
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"fmt"

	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (suite *WalletTestSuite) TestGRPCWallet() {
	walletId, _ := suite.SetupWalletAndSegment()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	wallet := testableApp.WalletKeeper.GetWallet(ctx, walletId)
	suite.Require().NotNil(wallet)

	var req *types.QueryGetWalletRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.EmptyRequest,
			func() {
				req = &types.QueryGetWalletRequest{}
			},
			false,
		},
		{
			app.ValidRequest,
			func() {
				req = &types.QueryGetWalletRequest{Id: walletId}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.Wallet(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
				suite.Equal(wallet.Id, res.Wallet.Id)
			} else {
				suite.ErrorIs(err, sdkErrors.ErrKeyNotFound)
				suite.Nil(res)
			}
		})
	}
}

func (suite *WalletTestSuite) TestGRPCAllWallet() {
	suite.SetupWalletAndSegment()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	wallet := testableApp.WalletKeeper.GetAllWallet(ctx)
	suite.Require().NotNil(wallet)

	var req *types.QueryAllWalletRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllWalletRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.WalletAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Nil(res)
			}
		})
	}
}

func (suite *WalletTestSuite) TestGRPCSegment() {
	_, segmentId := suite.SetupWalletAndSegment()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	segment := testableApp.WalletKeeper.GetSegment(ctx, segmentId)
	suite.Require().NotNil(segment)

	var req *types.QueryGetSegmentRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.EmptyRequest,
			func() {
				req = &types.QueryGetSegmentRequest{}
			},
			false,
		},
		{
			app.ValidRequest,
			func() {
				req = &types.QueryGetSegmentRequest{Id: segmentId}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.Segment(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Nil(res)
			}
		})
	}
}

func (suite *WalletTestSuite) TestGRPCAllSegment() {
	suite.SetupWalletAndSegment()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	segment := testableApp.WalletKeeper.GetAllSegment(ctx)
	suite.Require().NotNil(segment)

	var req *types.QueryAllSegmentRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllSegmentRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.SegmentAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Nil(res)
			}
		})
	}
}

func (suite *WalletTestSuite) TestGRPCAllWalletHistory() {
	suite.SetupWalletAndSegment()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	walletHistory := testableApp.WalletKeeper.GetAllWalletHistory(ctx)
	suite.Require().NotNil(walletHistory)

	var req *types.QueryAllWalletHistoryRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllWalletHistoryRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.WalletHistoryAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Nil(res)
			}
		})
	}
}

func (suite *WalletTestSuite) TestGRPCSegmentHistory() {
	_, segmentId := suite.SetupWalletAndSegment()
	segment := suite.keeper.GetSegment(suite.ctx, segmentId)
	segment.Creator = app.TokenCreator
	suite.keeper.SetSegment(suite.ctx, segment)
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	segmentHistory := testableApp.WalletKeeper.GetSegmentHistory(ctx, segmentId)
	suite.Require().NotNil(segmentHistory)

	var req *types.QueryGetSegmentHistoryRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.EmptyRequest,
			func() {
				req = &types.QueryGetSegmentHistoryRequest{}
			},
			false,
		},
		{
			app.ValidRequest,
			func() {
				req = &types.QueryGetSegmentHistoryRequest{Id: segmentId}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.SegmentHistory(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Require().Equal("", res.SegmentHistory.Id)
			}
		})
	}
}

func (suite *WalletTestSuite) TestGRPCAllSegmentHistory() {
	_, segmentId := suite.SetupWalletAndSegment()
	segment := suite.keeper.GetSegment(suite.ctx, segmentId)
	segment.Creator = app.TokenCreator
	suite.keeper.SetSegment(suite.ctx, segment)
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	segmentHistory := testableApp.WalletKeeper.GetAllSegmentHistory(ctx)
	suite.Require().NotNil(segmentHistory)

	var req *types.QueryAllSegmentHistoryRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllSegmentHistoryRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.SegmentHistoryAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Nil(res)
			}
		})
	}
}

func (suite *WalletTestSuite) TestGRPCTokenHistoryGlobal() {
	tokenHistoryGlobalId := suite.SetupTokenHistoryGlobal()
	tokenHistoryGlobal := suite.keeper.GetTokenHistoryGlobal(suite.ctx, tokenHistoryGlobalId)
	tokenHistoryGlobal.Creator = app.TokenCreator
	suite.keeper.SetTokenHistoryGlobal(suite.ctx, tokenHistoryGlobal)
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	TokenHistoryGlobal := testableApp.WalletKeeper.GetTokenHistoryGlobal(ctx, tokenHistoryGlobalId)
	suite.Require().NotNil(TokenHistoryGlobal)

	var req *types.QueryGetTokenHistoryGlobalRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.EmptyRequest,
			func() {
				req = &types.QueryGetTokenHistoryGlobalRequest{}
			},
			false,
		},
		{
			app.ValidRequest,
			func() {
				req = &types.QueryGetTokenHistoryGlobalRequest{Id: tokenHistoryGlobalId}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.TokenHistoryGlobal(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Require().Nil(res)
			}
		})
	}
}

func (suite *WalletTestSuite) TestGRPCAllTokenHistoryGlobal() {
	tokenHistoryGlobalId := suite.SetupTokenHistoryGlobal()
	tokenHistoryGlobal := suite.keeper.GetTokenHistoryGlobal(suite.ctx, tokenHistoryGlobalId)
	tokenHistoryGlobal.Creator = app.TokenCreator
	suite.keeper.SetTokenHistoryGlobal(suite.ctx, tokenHistoryGlobal)
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	TokenHistoryGlobal := testableApp.WalletKeeper.GetAllTokenHistoryGlobal(ctx)
	suite.Require().NotNil(TokenHistoryGlobal)

	var req *types.QueryAllTokenHistoryGlobalRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllTokenHistoryGlobalRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.TokenHistoryGlobalAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Nil(res)
			}
		})
	}
}
